# Setup project
### Install vendor dependencies

```
$ docker-compose run composer update
```

# How to run tests

```
# Run Behat:
$ docker-compose run behat
```
