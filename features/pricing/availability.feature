Feature: Availability
  In order to retrieve availability
  As an API client
  I need to be able to request availability

  Scenario: N/A collected from a single available Provider
    Given Total Providers giving availability is 1
    And I have the payload
    """
    checkin=2040-01-01&
    checkout=2040-01-08&
    adults[0]=2&
    children[0]=0&
    market=ES&
    ids=811&
    language=es
    """
    And Provider 1 is hotelbeds and returns
    """
    {"availability": []}
    """
    When I request availability
    Then the response status code should be 200
    And the availability response should be empty

  Scenario: N/A collected from all available Providers
    Given Total Providers giving availability is 3
    And I have the payload
    """
    checkin=2040-01-01&
    checkout=2040-01-08&
    adults[0]=2&
    children[0]=0&
    market=ES&
    ids=811&
    language=es
    """
    And Provider 1 is hotelbelds and returns
    """
    {"availability": []}
    """
    And Provider 2 is smyrooms and returns
    """
    {"availability": []}
    """
    And Provider 3 is webbeds and returns
    """
    {"availability": []}
    """
    When I request "POST /affiliates/pricing"
    Then the response status code should be 200
    And the availability response should be empty
